public class monitor {

    private char[] buffer = new char[6];
    private int hueco = 0;
    private boolean lleno = false;
    private boolean vacio =  true;

    public synchronized char sacar(){
        while (vacio == true){
            try{
                wait();
            }catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
        hueco --;
        if (hueco == 0){
            vacio = true;
        }
        lleno = false;
        notify();
        return (buffer[hueco]);
    }

    public synchronized void meter(char c){
        while (lleno == true){
            try{
                wait();
            }catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
        buffer[hueco] = c;
        hueco ++;
        if(hueco==6){
            lleno = true;
        }
        vacio = false;
        notify();
    }
}
