public class consumidor extends Thread {
    private monitor monitor;

    public consumidor(monitor m) {
        monitor = m;
    }

    public void run() {
        char c;
        for (int i = 0; i < 15; i++) {
            c = monitor.sacar();
            System.out.println("Cogida la letra " + c);
            try {
                sleep((int) (Math.random()*500));
            }catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}
