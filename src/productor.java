public class productor extends Thread{
    private monitor monitor;
    private String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public productor (monitor m){
        monitor = m;
    }

    public void run() {
        char c;
        for(int i = 0; i < 15; i++){
            c = letras.charAt((int)(Math.random()*26));
            monitor.meter(c);
            System.out.println("Enviado caracter "+c+" al monitor.");
            try{
                sleep((int)(Math.random()*100));
            }catch(InterruptedException ie){
                ie.printStackTrace();
            }
        }
    }
}
